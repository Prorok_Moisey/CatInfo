import axios from 'axios';

import {ApiKey} from './configConst';

const api: any = axios.create({
  baseURL: 'https://api.thecatapi.com/v1/',
  headers: {
    get: {
      'x-api-key': ApiKey,
    },
  },
});

export async function fetchBreeds() {
  const result: any = await api.get('breeds');
  return result.data;
}

export async function fetchSomeBreeds(page: number, count: number) {
  const result: any = await api.get(`breeds?page=${page}&limit=${count}`);
  return result.data;
}

export async function fetchBreedImage(breedId: string) {
  const result: any = await api.get(`images/search?breed_id=${breedId}`);
  return result.data[0].url;
}
