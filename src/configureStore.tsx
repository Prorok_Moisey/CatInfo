import AsyncStorage from '@react-native-community/async-storage';
import {applyMiddleware, compose, createStore} from 'redux';
import {persistStore, persistCombineReducers} from 'redux-persist';
import createSagaMiddleware from 'redux-saga';
import Reactotron from '../reactotronConfig';

import reducers from './state/rootReducer';
import rootSaga from "./state/rootSaga";

function configureStore() {
  const appReducer = persistCombineReducers(
    {
      key: 'root',
      storage: AsyncStorage,
    },
    reducers,
  );
  const sagaMiddleware = createSagaMiddleware({sagaMonitor:  Reactotron.createSagaMonitor()});
  const store = createStore(appReducer, {}, compose(applyMiddleware(sagaMiddleware), Reactotron.createEnhancer()));

  const persistor = persistStore(store, null, () => {
    store.dispatch({type: 'READY'});
  });
  sagaMiddleware.run(rootSaga);
  return {store, persistor};
}

const {store, persistor} = configureStore();

export {store, persistor};
