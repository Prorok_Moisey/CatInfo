import React from 'react';
import {Router, Scene} from 'react-native-router-flux';

import MainScreen from './screens/MainScreen';
import BreedScreen from './screens/BreedScreen';

const Routes = () => (
  // <Router navigationBarStyle={{borderBottomWidth: 2, borderBottomColor: MainThemeColor}}>
  <Router>
    <Scene key="root">
      <Scene
        key="main"
        component={MainScreen}
        title="Cat breeds"
        titleStyle={{width: '100%'}}
        initital={true}
      />
      <Scene key="breed" component={BreedScreen} title="Cat" />
    </Scene>
  </Router>
);

export default Routes;
