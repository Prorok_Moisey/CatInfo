import AsyncStorage from '@react-native-community/async-storage';
import {persistReducer} from 'redux-persist';

import {
  persistConfig as breedConfig,
  reducer as breedReducer,
} from './reducers/breeds';

const setStorage = (config: {key: string}) =>
  Object.assign({}, config, {storage: AsyncStorage});

export default {
  breeds: persistReducer(setStorage(breedConfig), breedReducer),
};
