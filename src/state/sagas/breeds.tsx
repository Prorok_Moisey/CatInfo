import {call, put, takeEvery, takeLatest} from 'redux-saga/effects';

import {
  ACTION_BREEDS_LOAD_IMAGE_START,
  ACTION_BREEDS_LOAD_START,
  ACTION_BREEDS_RESET_START,
  Breed,
  breedsLoadFailure,
  breedsLoadImageFailure,
  breedsLoadImageStart,
  BreedsLoadImageStartAction,
  breedsLoadImageSuccess,
  BreedsLoadStartAction,
  breedsLoadSuccess,
  breedsResetFailure,
  BreedsResetStartAction,
  breedsResetSuccess,
} from '../reducers/breeds';
import {fetchBreedImage, fetchBreeds, fetchSomeBreeds} from '../../api';
import {NumberToLoad} from "../../configConst";

// function* loadBreeds() {
//   try {
//     let data;
//     yield (data = fetchBreeds());
//     const newData: Array<Breed> = data._W.map((item: any) => {
//       return {
//         id: item.id,
//         name: item.name,
//         url: 'bla-bla',
//         wiki: item.wikipedia_url,
//         description: item.description,
//       };
//     });
//     yield put(breedsPush(newData));
//   } catch (error) {
//     yield console.log('err', error);
//   }
// }

function prepareBreedsData(data: any): Breed[] {
  return data.map((item: any) => {
    return new Breed(
      item.id,
      item.name,
      item.image.url,
      item.wikipedia_url,
      item.description,
    );
  });
}

function* loadBreeds(action: BreedsLoadStartAction) {
  try {
    const {page, count} = action.payload;
    // yield delay(200);
    const data = yield call(() => fetchSomeBreeds(page, count));
    if (!data) {
      yield put(breedsLoadSuccess({nextPage: 'exit', breeds: null}));
    } else {
      const newBreeds: Array<Breed> = prepareBreedsData(data);
      yield put(
        breedsLoadSuccess({
          nextPage:
            data.length < count
              ? 'exit'
              : data.length > NumberToLoad
              ? page + 2
              : page + 1,
          breeds: newBreeds,
        }),
      );
    }
  } catch (error) {
    yield console.log('error load breeds:', error);
    yield put(breedsLoadFailure());
  }
}

function* loadBreedImage(action: BreedsLoadImageStartAction) {
  try {
    const {breedId} = action.payload;
    const url = yield call(() => fetchBreedImage(breedId));
    yield put(breedsLoadImageSuccess({breedId, url}));
  } catch (err) {
    yield console.log('load image error:', err);
    yield put(breedsLoadImageFailure());
  }
}

function* resetBreeds(action: BreedsResetStartAction) {
  try {
    const {count} = action.payload;
    const page = 0;
    const data = yield call(() => fetchSomeBreeds(page, count));
    if (!data) {
      yield put(breedsResetFailure());
    } else {
      const newBreeds: Array<Breed> = prepareBreedsData(data);
      yield put(
        breedsResetSuccess({
          nextPage:
            data.length < count
              ? 'exit'
              : data.length > NumberToLoad
              ? page + 2
              : page + 1,
          breeds: newBreeds,
        }),
      );
    }
  } catch (error) {
    yield console.log('error reset breeds:', error);
    yield put(breedsResetFailure());
  }
}

export default [
  takeLatest<BreedsLoadStartAction>(ACTION_BREEDS_LOAD_START, loadBreeds),
  takeLatest<BreedsResetStartAction>(ACTION_BREEDS_RESET_START, resetBreeds),
  // takeEvery<BreedsLoadImageStartAction>(
  //   ACTION_BREEDS_LOAD_IMAGE_START,
  //   loadBreedImage,
  // ),
];
