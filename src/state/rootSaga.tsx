import {all} from 'redux-saga/effects';

import breedsSagas from './sagas/breeds';

export default function* rootSaga() {
  yield all([...breedsSagas]);
}
