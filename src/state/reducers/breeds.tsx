import {NumberToLoad} from '../../configConst';

// Actions
export const ACTION_BREEDS_PUSH: string = 'cat_info/breeds/push';

export const ACTION_BREEDS_LOAD_START: string = 'cat_info/breeds/load/start';
export const ACTION_BREEDS_LOAD_SUCCESS: string =
  'cat_info/breeds/load/success';
export const ACTION_BREEDS_LOAD_FAILURE: string =
  'cat_info/breeds/load/failure';

export const ACTION_BREEDS_RESET_START: string = 'cat_info/breeds/reset/start';
export const ACTION_BREEDS_RESET_SUCCESS: string =
  'cat_info/breeds/reset/success';
export const ACTION_BREEDS_RESET_FAILURE: string =
  'cat_info/breeds/reset/failure';

export const ACTION_BREEDS_LOAD_IMAGE_START: string =
  'cat_info/breeds/load/image/start';
export const ACTION_BREEDS_LOAD_IMAGE_SUCCESS: string =
  'cat_info/breeds/load/image/success';
export const ACTION_BREEDS_LOAD_IMAGE_FAILURE: string =
  'cat_info/breeds/load/image/failure';

// Variables
export class Breed {
  public id: string;
  public name: string;
  public url: string;
  public wiki: string;
  public description: string;
  constructor(
    id: string,
    name: string,
    url: string,
    wiki: string,
    description: string,
  ) {
    this.id = id;
    this.name = name;
    this.url = url;
    this.wiki = wiki;
    this.description = description;
  }
}

let breedArray: Array<Breed> = [];

// State
export class BreedsState {
  public breedsList: Array<Breed> | [];
  public isBreedsLoading: boolean;
  public isBreedsResetting: boolean;
  public page: number;
  constructor() {
    this.breedsList = breedArray;
    this.isBreedsLoading = false;
    this.isBreedsResetting = false;
    this.page = 0;
  }
}

// Persist configuration
export const persistConfig = {
  key: 'breeds',
};

export function reducer(state: BreedsState = new BreedsState(), action: any) {
  switch (action.type) {
    case ACTION_BREEDS_LOAD_START:
      return {...state, isBreedsLoading: true};
    case ACTION_BREEDS_LOAD_SUCCESS:
      return {
        ...state,
        isBreedsLoading: false,
        page: action.payload.nextPage,
        breedsList: action.payload.breeds
          ? state.breedsList.concat(action.payload.breeds)
          : state.breedsList,
      };
    case ACTION_BREEDS_LOAD_FAILURE:
      return {...state, isBreedsLoading: false};
    case ACTION_BREEDS_RESET_START:
      return {...state, isBreedsResetting: true};
    case ACTION_BREEDS_RESET_SUCCESS:
      return {
        ...new BreedsState(),
        isBreedsResetting: false,
        page: action.payload.nextPage,
        breedsList: action.payload.breeds
          ? new BreedsState().breedsList.concat(action.payload.breeds)
          : new BreedsState().breedsList,
      };
    case ACTION_BREEDS_RESET_FAILURE:
      return {...state, isBreedsResetting: false};
    // case ACTION_BREEDS_PUSH:
    //   return {
    //     ...state,
    //     breedsList: state.breedsList.concat(action.breeds),
    //   };
    case ACTION_BREEDS_LOAD_IMAGE_SUCCESS:
      const idx = state.breedsList.findIndex(
        (elem: Breed) => elem.id === action.payload.breedId,
      );
      const withUrl = state.breedsList.splice(idx, 1)[0];
      console.log('BreedList=', state.breedsList);
      withUrl.url = action.payload.url;
      state.breedsList.splice(idx, 0, withUrl);
      const list = state.breedsList.slice();
      return {...state, breedsList: list};
    case ACTION_BREEDS_LOAD_IMAGE_START:
    case ACTION_BREEDS_LOAD_IMAGE_FAILURE:
    default:
      return {...state};
  }
}

// interface BreedsPushAction {
//   type: typeof ACTION_BREEDS_PUSH;
//   payload: {
//     breeds: Breed[];
//   };
// }
//
// export function breedsPush(payload: {breeds: Array<Breed>}): BreedsPushAction {
//   return {
//     type: ACTION_BREEDS_PUSH,
//     payload,
//   };
// }

export interface BreedsLoadStartAction {
  type: typeof ACTION_BREEDS_LOAD_START;
  payload: {
    page: number;
    count: number;
  };
}

export function breedsLoadStart(payload: {
  page: number;
  count: number;
}): BreedsLoadStartAction {
  return {
    type: ACTION_BREEDS_LOAD_START,
    payload,
  };
}

interface BreedsLoadData {
  nextPage: number | string;
  breeds: Breed[] | null;
}

interface BreedsLoadSuccessAction {
  type: typeof ACTION_BREEDS_LOAD_SUCCESS;
  payload: BreedsLoadData;
}

export function breedsLoadSuccess(
  payload: BreedsLoadData,
): BreedsLoadSuccessAction {
  return {
    type: ACTION_BREEDS_LOAD_SUCCESS,
    payload,
  };
}

interface BreedsLoadFailureAction {
  type: typeof ACTION_BREEDS_LOAD_FAILURE;
}

export function breedsLoadFailure(): BreedsLoadFailureAction {
  return {type: ACTION_BREEDS_LOAD_FAILURE};
}

export interface BreedsResetStartAction {
  type: typeof ACTION_BREEDS_RESET_START;
  payload: {
    count: number;
  };
}

export function breedsResetStart(payload: {
  count: number;
}): BreedsResetStartAction {
  return {
    type: ACTION_BREEDS_RESET_START,
    payload,
  };
}

interface BreedsResetSuccessAction {
  type: typeof ACTION_BREEDS_RESET_SUCCESS;
  payload: BreedsLoadData;
}

export function breedsResetSuccess(
  payload: BreedsLoadData,
): BreedsLoadSuccessAction {
  return {
    type: ACTION_BREEDS_RESET_SUCCESS,
    payload,
  };
}

interface BreedsResetFailureAction {
  type: typeof ACTION_BREEDS_RESET_FAILURE;
}

export function breedsResetFailure(): BreedsResetFailureAction {
  return {type: ACTION_BREEDS_RESET_FAILURE};
}

export interface BreedsLoadImageStartAction {
  type: typeof ACTION_BREEDS_LOAD_IMAGE_START;
  payload: {
    breedId: string;
  };
}

export function breedsLoadImageStart(payload: {
  breedId: string;
}): BreedsLoadImageStartAction {
  return {
    type: ACTION_BREEDS_LOAD_IMAGE_START,
    payload,
  };
}

interface BreedsLoadImageSuccessAction {
  type: typeof ACTION_BREEDS_LOAD_IMAGE_SUCCESS;
  payload: {
    breedId: string;
    url: string;
  };
}

export function breedsLoadImageSuccess(payload: {
  breedId: string;
  url: string;
}): BreedsLoadImageSuccessAction {
  return {
    type: ACTION_BREEDS_LOAD_IMAGE_SUCCESS,
    payload,
  };
}

interface BreedsLoadImageFailureAction {
  type: typeof ACTION_BREEDS_LOAD_IMAGE_FAILURE;
}

export function breedsLoadImageFailure(): BreedsLoadImageFailureAction {
  return {
    type: ACTION_BREEDS_LOAD_IMAGE_FAILURE,
  };
}
