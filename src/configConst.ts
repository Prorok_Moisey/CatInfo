export const MainThemeColor: string = '#e88989';
export const SecondaryThemeColor: string = '#ffe3e3';
export const MainThemeBorderColor: string = '#c76f6f';
export const ApiKey: string = '1f2e7a16-907d-43a2-8ba4-0846e3350bef';
export const NumberToLoad: number = 6;
export const NumberToFirstLoad: number = 12;
export const TriggerLoadHeight: number = 100;
