/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */


if(__DEV__) {
  import('../reactotronConfig').then(() => console.log('Reactotron Configured'));
}

import React from 'react';
import Routes from './Routes';
import {Provider} from 'react-redux';

import {store, persistor} from './configureStore';
import {PersistGate} from 'redux-persist/lib/integration/react';

const App = () => {
  return (
    <>
      <Provider store={store}>
        <PersistGate persistor={persistor}>
          <Routes />
        </PersistGate>
      </Provider>
    </>
  );
};

export default App;
