import React, {Component} from 'react';
import {TouchableWithoutFeedback, View, Image, Text} from 'react-native';
import styled from 'styled-components';

import {MainThemeColor, MainThemeBorderColor} from "../configConst";

const Container = styled.View`
  height: 100;
  width: 100%;
  margin-top: 10;
  margin-bottom: 10;
  flex-direction: row;
  justify-content: flex-start;  
  align-items: center;  
  background-color: ${MainThemeColor};
  border-radius: 4;
`;

const Preview = styled.Image`
  height: 90;
  width: 90;
  margin: 5px;
  background-color: white;
  border-radius: 2;
`

const Title = styled.Text`
  flex: 1;
  height: 30;
  font-size: 24;
  text-align: center;
`;

type CardProps = {
  title: string;
  func: any;
  url: string | undefined;
};

class MenuCard extends Component<CardProps> {
  render() {
    return (
      <TouchableWithoutFeedback onPress = {this.props.func}>
        <Container>
          <Preview source={this.props.url ? {uri: this.props.url} : require('../images/question.png')} />
          <Title>
            { this.props.title }
          </Title>
        </Container>
      </TouchableWithoutFeedback>
    );
  }
}

export default MenuCard;
