import React, {Component} from 'react';
import styled from 'styled-components';
import {View, Image} from 'react-native';

const SpinnerImage = styled.Image`
  height: 40;
  width: 40;
  opacity: ${props => (props.isLoading ? 1 : 0)};
`

const SpinnerView = styled.View`
  width: 100%;
  justify-content: center;
  align-items: center;
  padding: 5px 5px 15px 5px;
`

type SpinnerProps = {
  isLoading: boolean;
}

class Spinner extends Component<SpinnerProps> {
  render() {
    return(
      <SpinnerView>
        <SpinnerImage
          isLoading={this.props.isLoading}
          source={require('../images/spinner.gif')}
        />
      </SpinnerView>
    );
  }
}

export default Spinner;
