import React, {Component} from 'react';
import {Dimensions, ScrollView, View, RefreshControl} from 'react-native';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import {NumberToLoad, TriggerLoadHeight, MainThemeColor, NumberToFirstLoad} from '../configConst';

import MenuCard from '../components/MenuCard';
import Spinner from '../components/Spinner';
import {
  Breed,
  breedsLoadStart,
  breedsResetStart,
} from '../state/reducers/breeds';

type MainScreenProps = {
  breedsList: Breed[];
  page: number | string;
  isLoading: boolean;
  isResetting: boolean;
  // loadBreeds: (page: number | string, count: number) => void;
  // resetBreeds: (count: number) => void;
};

class MainScreen extends Component<MainScreenProps> {
  constructor(props: Readonly<MainScreenProps>) {
    super(props);
    if (this.props.breedsList.length === 0) {
      this.props.loadBreeds(this.props.page, NumberToFirstLoad);
    }
  }
  handleScroll(event: any): void {
    if (
      this.props.page !== 'exit' &&
      !this.props.isLoading &&
      !this.props.isResetting
    ) {
      let offsetX: number = event.nativeEvent.contentOffset.y;
      let screenHeight: number = Dimensions.get('window').height;
      let contentHeight: number = event.nativeEvent.contentSize.height;
      if (contentHeight - screenHeight - offsetX < TriggerLoadHeight) {
        this.props.loadBreeds(this.props.page, NumberToLoad);
      }
    }
  }
  render() {
    const goToBreed = (item: Breed) => {
      Actions.breed({
        title: item.name,
        description: item.description,
        url: item.url,
        wiki: item.wiki,
      });
    };
    const onRefresh = () => {
      this.props.resetBreeds(NumberToFirstLoad);
    };
    return (
      <RefreshControl
        style={{width: '100%', height: '100%'}}
        refreshing={this.props.isResetting}
        onRefresh={onRefresh}
        colors={[MainThemeColor]}>
        <ScrollView
          onScroll={(event) => this.handleScroll(event)}
          style={{width: '100%', flex: 1}}
          contentContainerStyle={{alignItems: 'center'}}>
          <View style={{width: '90%'}}>
            {this.props.breedsList.length > 0 &&
              this.props.breedsList.map((item: Breed) => (
                <MenuCard
                  key={item.id}
                  title={item.name}
                  func={() => goToBreed(item)}
                  url={item.url}
                />
              ))}
            <Spinner isLoading={this.props.isLoading} />
          </View>
        </ScrollView>
      </RefreshControl>
    );
  }
}

const mapStateToProps = (state: any) => ({
  breedsList: state.breeds.breedsList,
  page: state.breeds.page,
  isLoading: state.breeds.isBreedsLoading,
  isResetting: state.breeds.isBreedsResetting,
});

const mapDispatchToProps = (dispatch: any) => ({
  loadBreeds: (page: number, count: number) => {
    dispatch(breedsLoadStart({page, count}));
  },
  resetBreeds: (count: number) => {
    dispatch(breedsResetStart({count}));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(MainScreen);
