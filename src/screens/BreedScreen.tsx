import React, {Component, useCallback} from 'react';
import {TouchableWithoutFeedback, ScrollView, View, Image, Text, Linking, Alert} from 'react-native';
import styled from 'styled-components';
import {Actions} from 'react-native-router-flux';

import {MainThemeColor, MainThemeBorderColor, SecondaryThemeColor} from "../configConst";

const Photo = styled.Image`
  height: 240;
  width: 240;
  border-width: 2;
  border-color: ${MainThemeColor};
  border-radius: 4px;
  margin-left: auto;
  margin-right: auto;
  margin-top: 40;
`;

const Label = styled.Text`  
  border-left-width: 2;
  border-left-color: ${MainThemeColor};
`;

const Title = styled.Text`
  font-size: 24;
  margin-top: 20;
  
`;

const Description = styled.Text`
  font-size: 18;
  margin-top: 20;
  line-height: 24;
`;

const Buttons = styled.View`
  width: 100%;
  flex-direction: row;
  justify-content: space-around;
  margin-top: 30;
  
`;

const Button = styled.Text`
  font-size: 18;
  border-width: 2;
  border-color: ${MainThemeColor};
  border-radius: 25;  
  padding: 10px;
`;

type BreedProps = {
  title: string;
  description: string;
  url: string;
  wiki: string;
};

class BreedScreen extends Component<BreedProps> {
  render() {
    const goToMain = () => {
      Actions.pop();
    }
    const OpenURLButton = () => {
      const handlePress = useCallback(async () => {
        // Checking if the link is supported for links with custom URL scheme.
        const supported = await Linking.canOpenURL(this.props.wiki);

        if (supported) {
          // Opening the link with some app, if the URL scheme is "http" the web link should be opened
          // by some browser in the mobile
          await Linking.openURL(this.props.wiki);
        } else {
          Alert.alert(`Don't know how to open this URL: ${this.props.wiki}`);
        }
      }, [this.props.wiki]);

      return <TouchableWithoutFeedback onPress={handlePress}><Button>More info</Button></TouchableWithoutFeedback>;
    };
    return (
      <ScrollView
        style={{width: '100%', flex: 1}}
        contentContainerStyle={{alignItems: 'center'}}>
        <View style={{width: '90%', paddingBottom: 10}}>
          <Photo source={this.props.url ? {uri: this.props.url} : require('../images/question.png')}/>
          <Title><Label>Breed: </Label>{this.props.title}</Title>
          <Description><Label>Description: </Label>{this.props.description}</Description>
          <Buttons>
            <OpenURLButton/>
            <TouchableWithoutFeedback onPress={goToMain}>
              <Button>To breed list</Button>
            </TouchableWithoutFeedback>
          </Buttons>
        </View>
      </ScrollView>
    );
  }
}

export default BreedScreen;
